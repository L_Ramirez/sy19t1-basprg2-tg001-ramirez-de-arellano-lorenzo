#include <iostream>
#include <string>

using namespace std;

void swap(int* x, int* y) {
	int t = *x;
	*x = *y;
	*y = t;
}

void arraySort(int arr[]){
	for (int z = 0; z < 6; z++) {
		for (int y = 0; y < 6 - z; y++) {
			if (arr[y] > arr[y + 1]) {
				swap(&arr[y], &arr[y + 1]);
			}
		}
	}
}




void packageSuggestion(int& playerGold, int itemPrice) {
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	string choice;
	int x = 0;
	arraySort(packages);

	cout << "\nYou do not have enough gold to purchase that item\n";

	if ((itemPrice - playerGold) <= 150) {
		x = 0;
	}
	else if (((itemPrice - playerGold) <= 500) && ((itemPrice - playerGold) > 150)) {
		x = 1;
	}
	else if (((itemPrice - playerGold) <= 1780) && ((itemPrice - playerGold) > 500)) {
		x = 2;
	}
	else if (((itemPrice - playerGold) <= 4050) && ((itemPrice - playerGold) > 1780)) {
		x = 3;
	}
	else if (((itemPrice - playerGold) <= 13333) && ((itemPrice - playerGold) > 4050)) {
		x = 4;
	}
	else if (((itemPrice - playerGold) <= 30750) && ((itemPrice - playerGold) > 13333)) {
		x = 5;
	}
	else if (((itemPrice - playerGold) <= 250000) && ((itemPrice - playerGold) > 30750)) {
		x = 6;
	}
	else if ((itemPrice - playerGold) > 250000) {
		cout << " There is no package that will allow you to immediately purchase this item.\n" << "Please select another item.";
		return;
	}

	cout << "Would you like to purchase package " << x << " to complete your purchase? Y/N\n";
	cout << "Package contains: " << packages[x] << " gold.";
	cin >> choice;
	if ((choice == "Yes") || (choice == "Y")) {
		playerGold += packages[x];
		playerGold -= itemPrice;
		cout << "Thank you for your purchase! Your gold is now: " << playerGold << "\n";
		
		return;
	}
	else if ((choice == "No") || (choice == "N")) {
		cout << "Transaction canceled. Returning to price retrival. \n";
		return;
	}
}



void transaction(int& playerGold, int itemPrice) {
	if (itemPrice <= playerGold) {
		playerGold = playerGold - itemPrice;
		cout << "Thank you for your purchase! Your gold is now: " << playerGold << "\n";
		return;
	}
	else if (itemPrice > playerGold) {
		packageSuggestion(playerGold, itemPrice);
		return;
	}
}




int main() {
	int playerGold = 250;
	int itemPrice = 0;
	string choice;
	while (true) {
		int itemPrice = 0;
		cout << "Player's Gold:" << playerGold;
		cout << "\nWhat is the price of the item you wish to purchase? \n";
		cin >> itemPrice;
		transaction(playerGold, itemPrice);
		cout << "Player gold:" << playerGold;
		cout << "\nWould you like to make another transaction? Y/N\n";
		cin >> choice;
		if (choice == "Yes" || choice == "Y") {

		}
		else if ((choice == "No") || (choice == "N")) {
			cout << "Thank you for your patronage!";
			break;
		}
	}
}