#include "Unit.h"
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

Unit Unit::unitP(string name)
{
	int choice;
	mName = name;
	mLvl = 0;
	mAtk = 10;
	mMaxHp = 20;
	mHP = mMaxHp;
	mAgi = 5;
	mDex = 5;
	mVit = 5;
	system("cls");
	cout << "What class would you like to be? \n";
	cout << "[1] Warrior [2] Mage [3]Assassin \n";
	cin >> choice;
	switch (choice) {
		case 1:
			mJob = "Warrior";
			break;
		case 2:
			mJob = "Mage";
			break;
		case 3:
			mJob = "Assassin";
			break;
		default:
			cout << "Error in input. Terminating application.";
			mJob = "Error";
}
	return Unit();
}


bool Unit::advantageCheck(Unit * target)
{
	if ((mJob == "Warrior" && target->getClass() == "Assassin") || (mJob == "Mage" && target->getClass() == "Warrior") || (mJob == "Assassin" && target->getClass() == "Mage"))
	{
		return true;
	}
	else {
		return false;
	}
}


Unit Unit::unitC()
{
	int job = 0;
	srand(time(0));
	job = rand() % 3 + 1;
	switch (job) {
	case 1:
		mJob = "Warrior";
		break;
	case 2:
		mJob = "Mage";
		break;
	case 3:
		mJob = "Assassin";
		break;
	}
		mName = "Enemy " + mJob;
		mLvl = 0;
		mAtk = 10;
		mMaxHp = 20;
		mHP = mMaxHp;
		mAgi = 5;
		mDex = 5;
		mVit = 5;
	return Unit();
}


void Unit::attack(Unit* target)
{
	if (target->mHP == 0) {
		return;
	}
	else if (mHP == 0) {
		return;
	}
	
	if (hitCheck(target)==true) {
		int damage = 0;
		damage = mAtk - target->mVit;
		if (advantageCheck(target)) {
			damage += damage * 50 / 100;
		}
		if (damage >= 0) {
			damage = 1;
		}
		target->takeDamage(damage);
		cout << mName << " attacks! "<< target->getName() << " takes " << damage << " damage";
		system("PAUSE");
	}
	else {
		cout << mName << " missed!";
		system("PAUSE");
	}
	target->attack(this);

}

void Unit::takeDamage(int damage)
	{
		
		mHP -= damage;
		if (mHP < 0) {
			mHP = 0;
		}
		return;
	}

int Unit::getHp()
{
	return mHP;
}

void Unit::displayStats()
{
	cout << "Name: "<< mName << endl;
	cout << "Class: " << mJob << endl;
	cout << "Level: " << mLvl << endl;
	cout << "Hp: " << mHP << " / "<< mMaxHp << endl;
	cout << "Attack: " << mAtk << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
	cout << "Vitality: " << mVit << endl;

}

bool Unit::hitCheck(Unit* target)
{
	srand(time(0));
	int hitRate = 0;
	int hitCheck = 0;
	hitRate = ((mDex / target->getAgi()) * 100);
	
	hitCheck = (rand() % 100 + 1);

	if (hitRate >= 80) {
		hitRate = 80;
	}
	else if (hitRate <= 20) {
		hitRate = 20;
	}
	if (hitCheck <= hitRate) {
		return true;
	}
	else
	{
		return false;
	}

}

void Unit::levelUp()
{
	mLvl++;
	if (mJob == "Warrior") {
		mMaxHp += 3;
		mHP += 3;
		mVit += 3;
	}
	else if (mJob == "Assassin") {
		mAgi += 3;
		mDex += 3;
	}
	else if (mJob == "Mage") {
		mAtk += 5;
	}
}

void Unit::heal()
{
	mHP += (mMaxHp * 30/100);
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mJob;
}

int Unit::getAgi()
{
	return mAgi;
}

Unit::~Unit()
{
}

