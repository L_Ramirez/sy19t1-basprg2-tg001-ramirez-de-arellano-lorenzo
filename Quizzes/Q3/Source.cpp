#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;

int main() {
	string name;
	int battles = 0;
	cout << "What is your name? ";
	cin >> name;
	Unit* player=new Unit;
	player->unitP(name);
	player->levelUp();
	player->displayStats();
	system("PAUSE");
	while (battles >= 0) {
		battles += 1;
		system("cls");
		cout << "Get ready for the next battle ";
		system("PAUSE");
		Unit* enemy = new Unit;
		enemy->unitC();
		for (int i = 0;i <= battles - 1; i++) {
			enemy->levelUp();
		}
		player->displayStats();
		cout << endl << " vs " << endl;
		enemy->displayStats();
		cout << endl << "Fight!" << endl;
		system("PAUSE");
		if (player->getAgi() >= enemy->getAgi()) {
			player->attack(enemy);
		}
		else
		{
			enemy->attack(player);
		}
		system("cls");
		if (enemy->getHp() < 1) {
			cout << "You Won!" << endl;
			system("PAUSE");
			cout << "You Leveled Up!" << endl;
			player->levelUp();
			player->displayStats();
			player->heal();
			system("PAUSE");
		}
		else if (player->getHp() < 1) {
			cout << "Your lost to " << enemy->getName()<< endl;
			cout << "You were victorious in " << battles << " battles" << endl;
			player->displayStats();
			battles = -1;
		}
		delete enemy;
		
	}
	
	delete player;
}