#pragma once
#include <string>
#include <iostream>
using namespace std;
class Unit
{
public: 
	
	Unit unitP(string name);
	Unit unitC();
	bool advantageCheck(Unit* target);
	void attack(Unit* target);
	void takeDamage(int damage);
	int getHp();
	void displayStats();
	bool hitCheck(Unit* target);
	void levelUp();
	void heal();
	string getName();
	string getClass();
	int getAgi();
	~Unit();

private: 
	string mName;
	int mLvl=0;
	int mHP=0;
	int mMaxHp=0;
	int mAtk=0;
	int mVit=0;
	int mAgi=0;
	int mDex=0;
	string mJob;

};

