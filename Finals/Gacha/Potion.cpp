
#include "Potion.h"

Potion::Potion()
{
	mItemName = "Potion";
	mItemValue = 30;
}

Potion::~Potion()
{
}

int Potion::itemEffect()
{
	return mItemValue;
}

string Potion::getName()
{
	return mItemName;
}
