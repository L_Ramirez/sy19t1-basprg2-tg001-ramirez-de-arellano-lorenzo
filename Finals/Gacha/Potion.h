#pragma once
#include "Item.h"
#include <string>
using namespace std;
class Potion :
	public Item

{
public:
	Potion();
	~Potion();
	//Apply item effect
	int itemEffect();
	string getName();

private:

	string mItemName;
	int mItemValue = 0;
};

