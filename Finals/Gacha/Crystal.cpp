#include "Crystal.h"

Crystal::Crystal()
{
	mItemName = "Crystal";
	mItemValue = 15;
}

Crystal::~Crystal()
{
}

int Crystal::itemEffect()
{
		return mItemValue;
}

string Crystal::getName()
{
	return mItemName;
}
