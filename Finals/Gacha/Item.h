#pragma once
#include <string>
using namespace std;
class Item
{
public:
	Item();
	~Item();
	//Apply item effect
	virtual int itemEffect();

	virtual string getName();


private:
	string mItemName;
	int mItemValue;
};

