#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "Bomb.h"
#include "Crystal.h"
#include "Item.h"
#include "Potion.h"
#include "Player.h"
#include "Rare_Item.h"

using namespace std;

int main() {
	srand(time(NULL));
	string name;
	cout << "What is your name? " << endl;
	cin >> name;
	Player* player = new Player(name);

	cout << "Rolls cost 5 crystals per roll."<< endl;
	cout << "You will roll until you run out of health or crystals."<< endl;
	system("PAUSE");
	system("cls");
	while ((player->getHealth() != 0) && (player->getCrystal() != 0) && (player->getRarePoints() < 100)) {
		player->displayStats();
		player->addCrystal(-5);
		int rng = rand() % 100 + 1;
		system("PAUSE");
		if (rng> 85)
		{
			Item* item = new Crystal();
			cout << "You rolled " << item->getName() << endl;
			cout << "You gained " << item->itemEffect() <<" crystals!!" << endl;
			player->addCrystal(item->itemEffect());
			player->addItems(item);
			cout << "Your crystal stash is now at " << player->getCrystal()<< endl;

		}
		else if (rng < 85 && rng >65) {

			Item* item = new Bomb();
			cout << "You rolled " << item->getName() << endl;
			cout << "You lost " << item->itemEffect() << " health!!!" << endl;
			player->addHealth(item->itemEffect());
			player->addItems(item);
			cout << "Your Health is now at " << player->getHealth() << endl;
		}
		else if (rng < 65 && rng >50) {
			Item* item = new Potion();
			cout << "You rolled " << item->getName() << endl;
			cout << "You gained " << item->itemEffect() << " health!"<<endl;
			player->addHealth(item->itemEffect());
			player->addItems(item);
			cout << "Your health is now at " << player->getHealth() << endl;
		}
		else if (rng < 50) {
			rng = rand() % 100 + 1;
			Item* item = new Rare_Item(rng);

			cout << "You rolled a " << item->getName() << endl;
			cout << "You gained " << item->itemEffect() << " rare points!!" << endl;
			player->addRarePoints(item->itemEffect());
			player->addItems(item);
		}
		system("PAUSE");
		system("cls");
	}
	cout << "The game has ended."<< endl;
	player->displayStats();
	cout << endl;
	player->printItems();
	delete player;
	system("pause");
}


