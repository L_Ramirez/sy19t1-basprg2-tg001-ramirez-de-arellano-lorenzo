#include "Player.h"

Player::Player(string name)
{
	mName = name;
	mHealth = 100;
	mCrystal = 100;
	mRarePoints = 0;
}

Player::~Player()
{
	//Delete items
	for (int i = 0; i < mItemsRolled.size(); i++)
	{
		delete mItemsRolled[i];
	}
	mItemsRolled.clear();
}


void Player::displayStats()
{
	cout << "Player Name: "<< mName<< endl;
	cout << "Rare Points: "<< this->getRarePoints() << endl;
	cout << "Health: "<< this->getHealth() << endl;
	cout << "Crystals: " << this->getCrystal() << endl;
}

void Player::addItems(Item* item)
{
	mItemsRolled.push_back(item);
}

void Player::printItems()
{
	cout << "Items Rolled: " << endl;
	for (int i = 0;i < mItemsRolled.size();i++) {
		cout << i << ". " << mItemsRolled[i]->getName()<< endl;
	}
}



int Player::getRarePoints()
{
	return mRarePoints;
}

void Player::addRarePoints(int value)
{
	mRarePoints += value;
}

int Player::getHealth()
{
	return mHealth;
}

void Player::addHealth(int value)
{
	mHealth += value;
	if (mHealth > 100) {
		mHealth = 100;
	}
	else if (mHealth < 0) {
		mHealth = 0;
	}
}

int Player::getCrystal()
{
	return mCrystal;
}

void Player::addCrystal(int value)
{
	mCrystal+= value;
}
