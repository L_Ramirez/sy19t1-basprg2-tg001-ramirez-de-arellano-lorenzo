#pragma once
#include "Item.h"
#include <string>
using namespace std;

class Crystal:
	public Item
{
public:
	Crystal();
	~Crystal();

	//Apply item effect
	int itemEffect();

	string getName();


private:

	string mItemName;
	int mItemValue = 0;
};

