#include "Rare_Item.h"

Rare_Item::Rare_Item(int rng)
{
	if (rng < 80) {
		//rare
		mItemName = "Rare Crystal";
		mItemValue = 5;
	}
	else if ((rng > 80) && (rng < 98)) {
		//srare
		mItemName = "SR Crystal";
		mItemValue = 30;
	}
	else {
		//ssrare
		mItemName = "SSR Crystal";
		mItemValue = 50;
	}
}

Rare_Item::~Rare_Item()
{
}

int Rare_Item::itemEffect()
{
	return mItemValue;
}

string Rare_Item::getName()
{
	return mItemName;
}
