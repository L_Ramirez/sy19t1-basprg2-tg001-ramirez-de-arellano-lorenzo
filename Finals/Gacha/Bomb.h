#pragma once
#include "Item.h"
#include <string>
using namespace std;
class Bomb :
	public Item
{
public:
	Bomb();
	~Bomb();
	//Apply item effect
	int itemEffect();
	string getName();

private:

	string mItemName;
	int mItemValue= 0;
};

