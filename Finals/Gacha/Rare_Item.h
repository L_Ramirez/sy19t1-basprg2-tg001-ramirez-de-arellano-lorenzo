#pragma once
#include "Item.h"
#include <string>
using namespace std;
class Rare_Item :
	public Item
{
public:
	Rare_Item(int rng);
	~Rare_Item();
	//Apply item effect
	int itemEffect();
	string getName();

private:

	string mItemName;
	int mItemValue = 0 ;
};

