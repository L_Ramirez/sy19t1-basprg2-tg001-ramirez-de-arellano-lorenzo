#pragma once
#include "Item.h"
#include <vector>
#include <string>
#include <iostream>
using namespace std;
class Player
{
public:
	Player(string name);
	~Player();

	
	void displayStats();

	void addItems(Item* item);
	void printItems();


	int getRarePoints();
	void addRarePoints(int value);

	int getHealth();
	void addHealth(int value);

	int getCrystal();
	void addCrystal(int value);

	
	
private:
	string mName;
	int mRarePoints =0;
	int mHealth = 0;
	int mCrystal= 0;	
	vector<Item*> mItemsRolled;
};

