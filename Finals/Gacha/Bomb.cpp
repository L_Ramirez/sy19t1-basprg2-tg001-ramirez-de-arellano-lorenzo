#include "Bomb.h"

Bomb::Bomb()
{
	mItemName = "Bomb";
	mItemValue = -15;
}

Bomb::~Bomb()
{
}

int Bomb::itemEffect()
{
	return mItemValue;
}

string Bomb::getName()
{
	return mItemName;
}
