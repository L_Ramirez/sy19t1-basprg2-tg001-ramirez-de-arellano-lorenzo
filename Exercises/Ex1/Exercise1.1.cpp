#include <iostream>
#include <string.h>

using namespace std;

int factorial(int x) {
	int y = 1;
		while (x > 0) {
			y  *= x;
			x -= 1;
		}
		return y;
}


int main() {
	int x;

	cout << "Enter the number you wish to get the factorial of: ";
	cin >> x;
	cout << factorial(x);
}