#include "Wizard.h"
#include <string>
#include <iostream>

using namespace std;

int main() {
	Wizard w1("Wizard1", 60, 30);
	Wizard w2("Wizard2", 60, 30);
	while (true) {
		w2.damaged(w1.castSpell());
		w1.damaged(w2.castSpell());
		if (w1.status() <= 0) {
			cout << w1.getName() << "was defeated.\n";

		}
		else if (w2.status() <= 0) {
			cout << w2.getName() << "was defeated.\n";

		}
	}
}

