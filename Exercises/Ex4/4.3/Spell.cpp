#include <string>
#include "Spell.h"

using namespace std;


Spell::Spell(string name,int damage, int mpCost) {
	mSpellName = name;
	mDamage = damage;
	mMpcost = mpCost;
}
int Spell::spellCast() {
	return mMpcost;
}
int Spell::spellHit() {
	return mDamage;
}
string Spell::getSpellname() {
	return mSpellName;
}