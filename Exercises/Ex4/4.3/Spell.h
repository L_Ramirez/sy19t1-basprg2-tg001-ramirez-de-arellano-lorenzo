#pragma once
#include <string>
using namespace std;
class Spell

{
public: 
	Spell(string name, int damage, int mpCost);
	int spellCast();
	string getSpellname();
	int spellHit();
private:
	string mSpellName;
	int mMpcost;
	int mDamage;
}
;

