#include "Wizard.h"
#include "Spell.h"
#include <string>
#include <iostream>

using namespace std;

Wizard::Wizard(string name, int hp, int mp) {
	mName = name;
	mHp = hp;
	mMp = mp;
}


int Wizard::castSpell() {
	int damage;
	Spell fire("Fireball", 5,6);
	casted(fire.spellCast());
	damage = fire.spellHit();
	cout << mName << " casts " << fire.getSpellname() << ". He deals " << damage << " damage\n";
	return damage;
}

int Wizard::status() {
	if (mHp = 0)
		return mMp;
	else if (mMp = 0)
		return mMp;
}


void Wizard::damaged(int value) {
	mHp -= value;
	if (mHp < 0) {
		mHp = 0;
	}
	return;
}

void Wizard::casted(int value) {
	mMp -= value;
	if (mMp < 0) {
		mMp = 0;
	}
	return;
}

string Wizard::getName() {
	return mName;
}