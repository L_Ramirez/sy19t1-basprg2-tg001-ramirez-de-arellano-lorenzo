#pragma once
#include <string>
using namespace std;
class Wizard
{
public:
	int castSpell();
	Wizard(string name, int hp, int mp);
	void damaged(int value);
	int status();
	void casted(int value);
	string getName();

private:
	string mName;
	int mHp;
	int mMp;



};

