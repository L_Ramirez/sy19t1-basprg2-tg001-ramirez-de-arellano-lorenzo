#pragma once
#ifndef TouphRing
#define TouphRing
#include <string>
using namespace std;

class TouphRing {
public:
	void AccessoryEffect() {
	}

private:
	string mName;
	int mAttack;
	int mAttackPercent;
	int mMagicAtk;
	int mDefense;
	int mDefensePercent;
	int mMagicDef;
	int mMagicDefPercent;



	void Materia(int Attack, int AttackPercent, int MagicAtk,int Defense, int DefensePercent, int MagicDef, int MagicDefPercent) {
		mAttack += Attack;
		mAttackPercent += AttackPercent;
		mMagicAtk += MagicAtk;
		mDefense += Defense;
		mDefensePercent += DefensePercent;
		mMagicDef += MagicDef;
		mMagicDefPercent += MagicDefPercent;
	}
};



#endif // !TouphRing