#pragma once
#ifndef WizardBracelet
#define WizardBracelet
#include <string>
using namespace std;

class WizardBracelet {
public:
	void ArmorEffect() {
	}

private:
	string name;
	int mDefense;
	int mDefensePercent;
	int mMagicDef;
	int mMagicDefPercent;



	void Materia(int Defense, int DefensePercent, int MagicDef, int MagicDefPercent) {
		mDefense += Defense;
		mDefensePercent += DefensePercent;
		mMagicDef += MagicDef;
		mMagicDefPercent += MagicDefPercent;
	}
};



#endif // !WizardBracelet