#pragma once
#include "Skill.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

class Iron_skin: public Skill
{
public: 
	Iron_skin();
	~Iron_skin();

	void applyBuff(Unit* target);
	string getName();
};

