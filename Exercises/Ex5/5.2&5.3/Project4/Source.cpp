#include "Skill.h"
#include "Concentration.h"
#include "Haste.h"
#include "Iron_skin.h"
#include "Heal.h"
#include "Might.h"
#include "Unit.h"
#include <vector>

using namespace std;

int main() {
	Unit* player = new Unit();
	player->skills[0] = new Might();
	player->skills[1] = new Heal();
	player->skills[2] = new Iron_skin();
	player->skills[3] = new Concentration();
	player->skills[4] = new Haste();

	player->castBuff();


	system("PAUSE");

}