#pragma once
#include "Skill.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

class Concentration:public Skill
{
public:
	Concentration();
	~Concentration();

	void applyBuff(Unit* target);

	string getName();
};

