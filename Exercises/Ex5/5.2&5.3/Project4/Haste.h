#pragma once
#include "Skill.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

class Haste:public Skill
{
public:
	Haste();
	~Haste();

	void applyBuff(Unit* target);
	string getName();
};

