#pragma once
#include "Skill.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

class Might: public Skill
{
public:
	Might();
	~Might();

	void applyBuff(Unit* target);
	string getName();

};

