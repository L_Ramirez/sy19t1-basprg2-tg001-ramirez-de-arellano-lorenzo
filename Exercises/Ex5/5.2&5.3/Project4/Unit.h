#pragma once
#include "Skill.h"
#include "Concentration.h"
#include "Haste.h"
#include "Iron_skin.h"
#include "Heal.h"
#include "Might.h"
#include <vector>
#include <iostream>
#include <string>


class Unit
{
public:
	Unit();
	~Unit();

	Skill* skills[5];
	

	void addHP(int value) {
		mHP += value;
		if (mHP >= 30) {
			mHP = 30;
		}
	}

	int getHP() {
		return mHP;
	}
	void addAtk(int value) {
		mAtk += value;
	}
	void addDex(int value) {
		mDex += value;
	}
	void addVit(int value) {
		mVit += value;
	}
	void addAgi(int value) {
		mAgi += value;
	}

	void castBuff();



private:
	int mHP;
	int mAtk;
	int mVit;
	int mAgi;
	int mDex;


};

