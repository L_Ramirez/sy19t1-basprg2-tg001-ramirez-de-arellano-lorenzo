#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;
class Skill
{
public:
	Skill();
	~Skill();

	virtual void applyBuff(Unit* target);
	virtual string getName() = 0;;
};

