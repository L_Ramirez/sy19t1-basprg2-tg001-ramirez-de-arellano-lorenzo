#pragma once
#include "Skill.h"
#include "Unit.h"
#include <string>
#include <iostream>

using namespace std;

class Heal:
	public Skill
{
public: 
	Heal();
	~Heal();

	void applyBuff(Unit* target);
	string getName();
};

