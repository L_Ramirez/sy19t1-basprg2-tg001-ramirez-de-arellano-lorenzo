#include "Circle.h"

Circle::Circle()
{
}

Circle::~Circle()
{
}

int Circle::getLength()
{
	return mLength;
}

void Circle::setLength(int value)
{
	mLength = value;
}

int Circle::getArea()
{
	int Area = (3.14 * (mLength * mLength));
	return Area;
}
