#include "Shape.h"

Shape::Shape()
{
}

Shape::~Shape()
{
}

string Shape::getName()
{
	return mName;
}

int Shape::getNumSides()
{
	return mNumSides;
}

void Shape::setName(string name)
{
	mName = name;

}

void Shape::setNumSides(int numSides)
{
	mNumSides = numSides;
}

void Shape::setLength(int value)
{
	mLength = value;
}

void Shape::setHeight(int value)
{
	mHeight = value;
}

int Shape::getArea()
{
	return 0;
}
