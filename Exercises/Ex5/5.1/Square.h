#pragma once
#include "Shape.h"
class Square: public Shape
{
public: 
	Square();
	~Square();

	int getLength();
	void setLength(int value);
	int getHeight();
	void setHeight(int value);
	int getArea() override;

private:
	int mLength;
	int mNumSides;
	int mHeight;
};

