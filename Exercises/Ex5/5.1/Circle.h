#pragma once
#include "Shape.h"
class Circle: public Shape
{
public: 
	Circle();
	~Circle();

	int getLength();
	void setLength(int value);
	int getArea() override;

private:
	int mLength=0;
	int mNumofSides=0;
};

