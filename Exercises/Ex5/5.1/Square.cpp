#include "Square.h"

Square::Square()
{
}

Square::~Square()
{
}

int Square::getLength()
{
	return mLength;
}

void Square::setLength(int value)
{
	mLength = value;
}

int Square::getHeight()
{
	return mHeight;
}

void Square::setHeight(int value)
{
	mHeight = value;
}

int Square::getArea()
{
	int Area = mLength * mHeight;
	return Area;
}
