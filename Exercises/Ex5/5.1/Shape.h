#pragma once
#include <string>
using namespace std;
class Shape
{
public: 
	Shape();
	~Shape();

	string getName();
	int getNumSides();

	void setName(string name);
	void setNumSides(int numSides);
	void setLength(int value);
	void setHeight(int value);

	virtual int getArea();

private: 
	string mName;
	int mNumSides;
	int mLength;
	int mHeight;

};

