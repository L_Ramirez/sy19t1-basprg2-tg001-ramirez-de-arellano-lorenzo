#include <iostream>
#include "Shape.h"
#include <vector>
#include "Square.h"
#include "Circle.h"

using namespace std;

int main() {
	vector<Shape*> shapes;
	Shape* square = new Square();
	shapes.push_back(square);
	

	square->setName("Square");
	square->setNumSides(4);
	square->setLength(5);
	square->setHeight(5);

	Shape* rectangle = new Square;
	shapes.push_back(rectangle);


	rectangle->setName("Rectangle");
	rectangle->setNumSides(4);
	rectangle->setLength(6);
	rectangle->setHeight(4);

	Shape* circle = new Circle;
	shapes.push_back(circle);

	
	circle->setName("Circle");
	circle->setLength(4);
	circle->setNumSides(0);


	for (int i = 0; i < 3; i++) {
		cout << "Name: " << shapes[i]->getName() << "\nSides: " << shapes[i]->getNumSides() << "\nArea: " << shapes[i]->getArea() << "\n\n";
	}
	system("PAUSE");
}