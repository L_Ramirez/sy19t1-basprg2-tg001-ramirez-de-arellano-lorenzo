#include "Unit.h"
#include <iostream>
#include "Haste.h"
#include "Concentration.h"
#include "Heal.h"
#include "Might.h"
#include "Iron_Skin.h"
using namespace std;

int main() {
	
	Unit* unit = new Unit();
	unit->skills[0] = new Heal();
	unit->skills[1] = new Might();
	unit->skills[2] = new Iron_Skin();
	unit->skills[3] = new Concentration();
	unit->skills[4] = new Haste();

}