#pragma once
#include "Buff.h"
#include "Haste.h"
#include "Concentration.h"
#include "Heal.h"
#include "Might.h"
#include "Iron_Skin.h"

class Unit
{
public:

	
	Unit();
	~Unit();
	Buff* skills[4];
	void castBuff();


private: 
	int mHP;
	int mPoW;
	int mVit;
	int mDex;
	int mAgi;
};

