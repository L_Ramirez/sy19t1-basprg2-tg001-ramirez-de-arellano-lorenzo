#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

// Creates a data structure that can hold multiple items.
struct Item {
	string name;
	int gold;
};


//function to randomly determine what item is found
void ItemFound(Item* foundItem) {
	srand(time(NULL));
	int random;
	Item items[5];

	items[0].name = "Mithril Ore";
	items[0].gold = 100;

	items[1].name = "Sharp Talon";
	items[1].gold = 50;

	items[2].name = "Thick Leather";
	items[2].gold = 25;

	items[3].name = "Jellopy";
	items[3].gold = 5;

	items[4].name = "Cursed Stone";
	items[4].gold = 0;

	random = rand() % 4 + 0;
	*foundItem = items[random];

}

//Function to Facilitate entering the dungeon and collecting gold
void enterDungeon(int* gold) {
	Item foundItem;
	bool repeat = true;
	int instancegold = 0;
	int multiplier = 1;
	char goFurther;
	cout << "You have entered the dungeon.";
	while (repeat == true) {
		cout << "Wandering around in the dungeon, you stumble upon an item on the ground. \n";
		ItemFound(&foundItem);
		cout << "\n" << foundItem.name << "\n" << foundItem.gold ;
		instancegold += (foundItem.gold * multiplier);
		if (foundItem.gold == 0) {
			cout << " The " << foundItem.name << " curses you and sends you out of the dungeon.\n";
			cout << " The items you picked up have disappeared due to the curse \n";
			instancegold = 0;
			system("PAUSE");
			break;

		}
		else {
			cout << " Your total haul will give you " << instancegold << " \n";
			cout << " Continue exploring? Y/N ";
			cin >> goFurther;
			if (goFurther == 'Y') {
				++multiplier;
				cout << "\n You delve further into the dungeon.";
				system("PAUSE");
			}
			else if (goFurther == 'N') {
				repeat = false;
				cout << "\n You exit the dungeon with " << instancegold << " \n";
				*gold += instancegold;
				cout << "You have " << *gold;
				system("PAUSE");
			}
		}
	}
}


int main() {
	int gold = 50;
	char enter;
	while ((gold > 0) && (gold < 500)) {
		cout << "You have " << gold << "gold ";
		cout << "Enter the dungeon? \n";
		cin >> enter;
		if (enter == 'Y') {
			gold -= 25;
			enterDungeon(&gold);
		}

	}
		if (gold <= 0) {
			cout << "You're out of money! Game Over! ";
		}
		else if (gold >= 500) {
			cout << "You've acquired 500 gold coins! Congratulations! You've won!";
		}
	}
