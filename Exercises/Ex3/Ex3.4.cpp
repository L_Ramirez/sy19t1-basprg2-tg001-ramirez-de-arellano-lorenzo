#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

// Creates a data structure that can hold multiple items.
struct Item {
	string name;
	int gold;
};


//function to randomly determine what item is found
void ItemFound(Item* foundItem) {
	srand(time(NULL));
	int random;
	Item items[5];

	items[0].name = "Mithril Ore";
	items[0].gold = 100;

	items[1].name = "Sharp Talon";
	items[1].gold = 50;

	items[2].name = "Thick Leather";
	items[2].gold = 25;

	items[3].name = "Jellopy";
	items[3].gold = 5;

	items[4].name = "Cursed Stone";
	items[4].gold = 0;

	random = rand() % 5 + 0;
	*foundItem = items[random];

}