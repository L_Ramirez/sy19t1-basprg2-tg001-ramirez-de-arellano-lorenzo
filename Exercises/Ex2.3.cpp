#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

//EX2.1
//returning void
//function used to take bet for payout from player gold
void betting(int& pGold, int& bet) {
	while (true) {
		cout << "\nHow much would you like to bet?";
		cin >> bet;
		if (bet < pGold) {
			pGold -= bet;
			cout << "\nYou bet " << bet;
			break;
		}
		else if (bet > pGold) {
			cout << "\nYou don't have enough money!. ";

		}
		else if (bet <= 0){
			cout << "You can't bet 0 or less!";
		}
	}
	return;
}

//function to roll dice for both player and computer
void diceRoll(int& pRoll, int& cRoll) {
	srand(time(NULL));
	int d1;
	int d2;

	d1 = rand() % 6 + 1;
	cout << "\nOpponent rolled " << d1;
	d2 = rand() % 6 + 1;
	cout << "\nOpponent rolled " << d2;
	cRoll = d1 + d2;
	d1 = rand() % 6 + 1;
	cout << "\nPlayer rolled " << d1;
	d2 = rand() % 6 + 1;
	cout << "\nPlayer rolled " << d2;
	pRoll = d1 + d2;
}

void payout(int& pGold, int& pRoll, int& cRoll,int& bet) {
	if ((pRoll == 2)&&(pRoll !=cRoll)) {
		cout << "Snake Eyes! You win 3x your bet";
		pGold += (bet * 3);
	}
	else if (pRoll > cRoll) {
		cout << "\nYou Win!";
		pGold += (bet * 2);
	else if (cRoll > pRoll) {
		cout << "\nYou lose";
	}
	else if (cRoll == pRoll) {
		cout << "\nDraw.";
		pGold += bet;
	}
}

int main() {
	int pGold = 3000;
	int bet;
	int pRoll;
	int cRoll;
	cout << "\nYour gold is " << pGold;
	betting(pGold, bet);
	cout << "\nYour bet is " << bet << "\nYou have " << pGold << "left";
	diceRoll(pRoll, cRoll);
	payout(pGold, pRoll, cRoll, bet);

	}

	
}